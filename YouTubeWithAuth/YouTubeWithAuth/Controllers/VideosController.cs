﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YouTubeWithAuth.Models;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections;
using YouTube.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace YouTubeWithAuth.Controllers
{

    public class VideosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Videos/Create
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public JsonResult getMusicVideos()
        {
            var videos = db.Videos.Where(x => x.Music== true).OrderByDescending(x => x.ViewsCounter).Take(6).ToList();
            var result = new List<MainPageVideos>();

            foreach (var video in videos)
            {
                var temp = new MainPageVideos();
                temp = Mapper.toMainPageVideos(video);               
                result.Add(temp);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getFamilyVideos()
        {
            var videos = db.Videos.Where(x => x.Family == true).OrderByDescending(x => x.ViewsCounter).Take(6).ToList();
            var result = new List<MainPageVideos>();

            foreach (var video in videos)
            {
                var temp = new MainPageVideos();
                temp = Mapper.toMainPageVideos(video);          
                result.Add(temp);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult createPlaylist(string name, int[] ids)
        {
            var playlist = new Playlist();

            playlist.Id = User.Identity.GetUserId();
            playlist.NamePlaylist = name;    
            playlist.YouTubeUser = db.Users.Where(x => x.Id == playlist.Id).First();

            playlist.Videos = new List<Video>();

            for (var i = 0; i < ids.Length; i++)            
            {
                var temp = new Video();
                var id = ids[i];
                temp = db.Videos.Where(x => x.VideoId == id).First();
                playlist.Videos.Add(temp);
            }

            db.Playlists.Add(playlist);
            db.SaveChanges();

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getMostViewedVideo()
        {
            var videos = db.Videos.OrderByDescending(x => x.ViewsCounter).Take(3).ToList();
            var result = new List<MostViwedVideos>();
            

            foreach(var video in videos)
            {
                var temp = new MostViwedVideos();
                temp = Mapper.toMostViwedVideos(video);               
                result.Add(temp);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getVideo(int id)
        {            
            var video = db.Videos.Where(x => x.VideoId == id).FirstOrDefault();
            video.ViewsCounter = video.ViewsCounter+ 1;
            db.Entry(video).State = EntityState.Modified;
            db.SaveChanges();

            var result = new ShowVideo();

            result = Mapper.toShowVideo(video);            
            result.Usercomments = new  List<EComment>();

            var comms = db.Comments.Where(x => x.VideoID == id).OrderByDescending(x=>x.CommentDate).ToList();

            foreach (var comm in comms)
            {                    
                var temp = new EComment();
                temp = Mapper.toEComment(comm);
                result.Usercomments.Add(temp); 
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult makeComment (string comment, int id)
        {
            var result = new Comment();
            result.Id = User.Identity.GetUserId();
            if (result.Id != null)
            {
                result.VideoID = id;
                result.DislikesCounter = 0;
                result.CommentDate = DateTime.Now;
                result.CommentText = comment;
                result.LikesCounter = 0;
                db.Comments.Add(result);
                db.SaveChanges();

                var temp = new EComment();

                temp.id = db.Comments.Where(x => x.YouTubeUser.Id == result.Id && x.VideoID == result.VideoID).OrderByDescending(x => x.CommentDate).First().CommentId;
                temp.commentDate = result.CommentDate;
                temp.commentText = result.CommentText;
                temp.dislikesCounter = result.DislikesCounter;
                temp.likesCounter = result.LikesCounter;
                temp.username = User.Identity.GetUserName();

                return Json(temp, JsonRequestBehavior.AllowGet);

            }
            else return Json("Login", JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [Authorize]
        public JsonResult likeVideo(int id)
        {
            var vide = db.Videos.Where(x => x.VideoId == id).First();
            vide.LikesCounter += 1;
            db.Entry(vide).State = EntityState.Modified;
            db.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult dislikeVideo(int id)
        {
            var vide = db.Videos.Where(x => x.VideoId == id).First();
            vide.DislikesCounter += 1;
            db.Entry(vide).State = EntityState.Modified;
            db.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult likeComment(int id)
        {
            var comment = db.Comments.Where(x => x.CommentId == id).First();
            comment.LikesCounter += 1;
            db.Entry(comment).State = EntityState.Modified;
            db.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult dislikeComment(int id)
        {
            var comment = db.Comments.Where(x => x.CommentId == id).First();
            comment.DislikesCounter += 1;
            db.Entry(comment).State = EntityState.Modified;
            db.SaveChanges();
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSearchResult(string searchValue)
        {
            var result = new searchResult();
            result.users = new List<UserPageProfile>();
            result.videos = new List<SideVideo>();

            var users = db.Users.Where(x => x.UserName.Contains(searchValue)).ToList();
            var videos = db.Videos.Where(x => x.Title.Contains(searchValue)).ToList();

            foreach(var user in users)
            {
                var temp = new UserPageProfile();
                temp.profileURL = user.AvatarURL;
                temp.username = user.UserName;
                temp.userID = user.Id;
                result.users.Add(temp);
            }
            foreach(var video in videos)
            {
                var temp = new SideVideo();
                temp.id = video.VideoId;
                temp.imageURL = video.CoverImageURL;
                temp.title = video.Title;
                temp.username = video.YouTubeUser.UserName;
                temp.views = video.ViewsCounter;
                result.videos.Add(temp);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // POST: Videos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadVideo (UploadVideoViewModel model)
        {
            var user = User.Identity.GetUserId();
            var dbVideo = new Video();

            

            dbVideo.Id = user;
            dbVideo.Title = model.Title;
            dbVideo.Description = model.Description;
            dbVideo.VideoContentURL = "temporraryValue";
            dbVideo.CoverImageURL = "temporraryValue";
            dbVideo.DislikesCounter = 0;
            dbVideo.LikesCounter = 0;
            dbVideo.ViewsCounter = 0;
            dbVideo.Art = model.Art;
            dbVideo.Celebrity = model.Celebrity;
            dbVideo.Family = model.Family;
            dbVideo.Movie = model.Movie;
            dbVideo.Music = model.Music;
            dbVideo.News = model.News;

            var result = db.Videos.Add(dbVideo);
            await db.SaveChangesAsync();

            var videoPath = "~/Videos/";
            var thumbnailPath= "../Thumbnails/";

            if (ModelState.IsValid)
            {
                result.CoverImageURL = SaveFile(thumbnailPath, model.thumbnail, result.VideoId);
                result.VideoContentURL = SaveFile(videoPath, model.video, result.VideoId);
                db.Entry(result).State = EntityState.Modified;
                await db.SaveChangesAsync();

                return RedirectToAction("Index","Home");
            }
            return View();
        }

        private string SaveFile(string path, HttpPostedFileBase file, int id)
        {
            var fileSavePath = "";
            var uploadedFile = file;
            var fileName = "";
            fileName = Path.GetExtension(uploadedFile.FileName);
            fileSavePath = Server.MapPath(path + id + fileName);
            uploadedFile.SaveAs(fileSavePath);

            return path + id + fileName;
        }

        public JsonResult getSideVideos()
        {
            var result = new List<SideVideo>();
            var videos = db.Videos.OrderByDescending(x => x.ViewsCounter).Take(10).ToList();

            foreach(var video in videos)
            {
                var temp = new SideVideo();
                temp.id = video.VideoId;
                temp.imageURL = video.CoverImageURL;
                temp.title = video.Title;
                temp.username = video.YouTubeUser.UserName;
                temp.views = video.ViewsCounter;
                result.Add(temp);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getUser(string id)
        {
            var user = db.Users.Where(x => x.Id == id).First();
            var result = new UserPageProfile();
            result.profileURL = user.AvatarURL;
            result.username = user.UserName;
            

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getUserVideos(string id)
        {
            var result = new List<SideVideo>();
            var videos = db.Videos.Where(x => x.Id == id).ToList();

            foreach(var video in videos)
            {
                var temp = new SideVideo();
                temp.id = video.VideoId;
                temp.imageURL = video.CoverImageURL;
                temp.views = video.ViewsCounter;
                temp.title = video.Title;
                temp.username = video.YouTubeUser.UserName;
                result.Add(temp);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getUserPlaylists(string id)
        {
            var result = new List<PlaylistsResult>();
            var videos = db.Playlists.Where(x => x.Id == id).ToList();

            foreach(var list in videos)
            {
                var temp = new PlaylistsResult();
                temp.id = list.PlaylistId;
                temp.image = list.Videos.First().CoverImageURL;
                temp.noVideos = list.Videos.Count;
                temp.title = list.NamePlaylist;
                result.Add(temp);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPlayListVideo(int id)
        {
            var result = new ShowVideo();
            var vid = db.Playlists.Where(x => x.PlaylistId == id).First().Videos.First();
            vid.ViewsCounter++;
            db.Entry(vid).State = EntityState.Modified;
            db.SaveChanges();

                result.description = vid.Description;
                result.imageURL = vid.CoverImageURL;
                result.noDislikes = vid.DislikesCounter;
                result.noLikes = vid.LikesCounter;
                result.noViews = ++vid.ViewsCounter;
                result.title = vid.Title;
                result.usernameV = vid.YouTubeUser.UserName;
                result.videoURL = vid.VideoContentURL;
                result.Usercomments = new List<EComment>();
                foreach(var comm in vid.UserComments)
                {
                    var temp = new EComment();
                    temp.commentDate = comm.CommentDate;
                    temp.commentText = comm.CommentText;
                    temp.dislikesCounter = comm.DislikesCounter;
                    temp.id = comm.CommentId;
                    temp.likesCounter = comm.LikesCounter;
                    temp.username = comm.YouTubeUser.UserName;
                    result.Usercomments.Add(temp);
                }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPlayListSideVideos(int id)
        {
            var result = new List<SideVideo>();
            var videos = db.Playlists.Where(x => x.PlaylistId == id).First().Videos.ToList();

            foreach (var video in videos)
            {
                var temp = new SideVideo();
                temp.id = video.VideoId;
                temp.imageURL = video.CoverImageURL;
                temp.title = video.Title;
                temp.username = video.YouTubeUser.UserName;
                temp.views = video.ViewsCounter;
                result.Add(temp);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
