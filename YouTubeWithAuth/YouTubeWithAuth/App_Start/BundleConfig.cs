﻿using System.Web;
using System.Web.Optimization;

namespace YouTubeWithAuth
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                     "~/Scripts/angular.min.js",
                     "~/Scripts/angular-route.js",
                     "~/AngularControllers/app.js",
                     "~/AngularControllers/mainController.js",
                     "~/AngularControllers/videoController.js",
                     "~/AngularControllers/userController.js",
                     "~/AngularControllers/searchController.js",
                     "~/AngularControllers/playlistController.js",
                     "~/AngularControllers/viewplaylistController.js"

               ));


                    // Use the development version of Modernizr to develop with and learn from. Then, when you're
                    // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
                    bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/Site.css",
                      "~/Content/myCSS.css"));
           
        }
    }
}
