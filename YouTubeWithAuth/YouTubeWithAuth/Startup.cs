﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YouTubeWithAuth.Startup))]
namespace YouTubeWithAuth
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
