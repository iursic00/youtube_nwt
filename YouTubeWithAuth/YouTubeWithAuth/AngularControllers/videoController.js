﻿app.controller("videoController", function ($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        method: 'POST',
        url: 'Videos/getVideo',
        data: {
            id: id
        }
    }).then(function (response) {

        $scope.video = response.data;
    })

    $scope.makeComment = function () {
        $http({
            method: 'POST',
            url: 'Videos/makeComment',
            data: {
                comment: $scope.userComment,
                id: id
            }
        }).then(function (response) {
            if (response.data != "Login") {
                $scope.video.Usercomments.push(response.data)
                $scope.userComment = "";
            }
        })
    }

    $scope.likeVideo = function () {

        $http({
            method: 'POST',
            url: 'Videos/likeVideo',
            data: {
                id: id
            }
        }).then(function (response) {
            if (response.data == "OK")
                $scope.video.noLikes = $scope.video.noLikes + 1;
            else alert("Error!!");

        })
    }

    $scope.dislikeVideo = function () {

        $http({
            method: 'POST',
            url: 'Videos/dislikeVideo',
            data: {
                id: id
            }
        }).then(function (response) {
            if (response.data == "OK")
                $scope.video.noDislikes = $scope.video.noDislikes + 1;
            else alert("Error!!");

        })
    }

    $scope.likeComment = function (temp) {

        $http({
            method: 'POST',
            url: 'Videos/likeComment',
            data: {
                id: $scope.video.Usercomments[temp].id
            }
        }).then(function (response) {
            if (response.data == "OK")
                $scope.video.Usercomments[temp].likesCounter += 1;
            else alert("Error!!");

        })
    };
    $scope.dislikeComment = function (temp) {
        $http({
            method: 'POST',
            url: 'Videos/dislikeComment',
            data: {
                id: $scope.video.Usercomments[temp].id
            }
        }).then(function (response) {
            if (response.data == "OK")
                $scope.video.Usercomments[temp].dislikesCounter += 1;
            else alert("Error!!");

        })
    };
    getSideVideos = function () {
        $http({
            method: 'GET',
            url: 'Videos/getSideVideos'
        }).then(function (response) {
            $scope.sideVideos = response.data;
        })
    }
    getSideVideos();

    $scope.showVideo = function (temp) {
        $http({
            method: 'POST',
            url: 'Videos/getVideo',
            data: {
                id: $scope.sideVideos[temp].id
            }
        }).then(function (response) {

            $scope.sideVideos[temp].views++;
            $scope.video = response.data;
        })
    }


});