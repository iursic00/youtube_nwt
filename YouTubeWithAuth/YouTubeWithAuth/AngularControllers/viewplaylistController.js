﻿app.controller('viewplaylistController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        method: 'POST',
        url: 'Videos/getPlayListVideo',
        data: {
            id: id
        }
    }).then(function (response) {

        $scope.video = response.data;
    })

    $http({
        method: 'POST',
        url: 'Videos/getPlayListSideVideos',
        data: {
            id: id
        }
    }).then(function (response) {

        $scope.sideVideos = response.data;
    })

    $scope.showVideo = function (videoID) {
        $http({
            method: 'POST',
            url: 'Videos/getVideo',
            data: {
                id: videoID
            }
        }).then(function (response) {

            $scope.video = response.data;
        })
    }

})