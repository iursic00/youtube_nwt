﻿app.controller('playlistController', function ($scope, $http) {
    $scope.playlistVideos = [];
    var ids=[];
    $scope.search = function () {
        $http({
            method: 'POST',
            url: 'Videos/getSearchResult',
            data: {
                searchValue: $scope.searchVal
            }
        }).then(function (response) {
            $scope.searchVal = "";
            $scope.videos = response.data.videos;
            videos = response.data.videos
        })
    }

    $scope.addToPlaylist = function (video) {

        ids.push(video.id);
        $scope.playlistVideos.push(video);
    }

    $scope.removeFromPlaylist = function (index) {
        $scope.playlistVideos.splice(index, 1);
    }
    $scope.creatPlaylist = function () {
        $http({
            method: 'POST',
            url: 'Videos/createPlaylist',
            data: {
                name: $scope.playlistName,
                ids: ids
            }
        }).then(function (response) {
                $scope.playlistVideos=[];
        })
    }
})
