﻿
var app = angular.module("YouTubeApp", ['ngRoute']);

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
        .when('/', {
            title: 'Main Page',
            templateUrl: '/AngularViews/VideosPage.html',
            controller: 'mainController'
        })
        .when('/ViewVideoPage/:id', {
            title: 'Upload Video',
            templateUrl: '/AngularViews/ViewVideoPage.html',
            controller: 'videoController'
        })
            .when('/UserPage/:id', {
            title: 'User Page',
            templateUrl: '/AngularViews/UserPage.html',
            controller: 'userController'
            })
            .when('/ChooseVideo/:searchVal', {
                title: 'Videos',
                templateUrl: '/AngularViews/ChooseVideo.html',
                controller: 'searchController'
            })
            .when('/CreatePlaylist', {
                title: 'Create playlist',
                templateUrl: '/AngularViews/CreatePlayList.html',
                controller: 'playlistController'
            })
            .when('/ViewPlaylistPage/:id', {
                title: 'View playlist',
                templateUrl: '/AngularViews/ViewPlaylistPage.html',
                controller: 'viewplaylistController'
            })
        .otherwise({
            redirectTo: 'Main Page'
        });
    }]);

