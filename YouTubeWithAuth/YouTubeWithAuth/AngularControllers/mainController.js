﻿app.controller("mainController", function ($scope, $http) {
    $scope.imageURL = "";
    $scope.videoURL = "";
    getMostViewedVideo = function () {
        $http({
            method: 'GET',
            url: 'Videos/getMostViewedVideo'
        }).then(function (response) {
            $scope.imageURLM = response.data[0].imageURL;
            $scope.titleM = response.data[0].Title;
            $scope.idM = response.data[0].id;

            $scope.imageURLF = response.data[1].imageURL;
            $scope.titleF = response.data[1].Title;
            $scope.NoViewsF = response.data[1].NoViews;
            $scope.idF = response.data[1].id;

            $scope.imageURLS = response.data[2].imageURL;
            $scope.titleS = response.data[2].Title;
            $scope.NoViewsS = response.data[2].NoViews;
            $scope.idS = response.data[2].id;
        })
    };

    getMusicVideos = function () {

        $http({
            method: 'GET',
            url: 'Videos/getMusicVideos'
        }).then(function (response) {
            $scope.musics = response.data;
        })
    };
    getFamilyVideos = function () {

        $http({
            method: 'GET',
            url: 'Videos/getFamilyVideos'
        }).then(function (response) {
            $scope.families = response.data;

        })
    };
    getMostViewedVideo();
    getMusicVideos();
    getFamilyVideos();
})