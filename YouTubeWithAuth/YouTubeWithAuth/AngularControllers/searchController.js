﻿app.controller('searchController', function ($scope, $http, $routeParams) {
    var searchValue = $routeParams.searchVal;

    $http({
        method: 'POST',
        url: 'Videos/getSearchResult',
        data: {
            searchValue: searchValue
        }
    }).then(function (response) {
        $scope.searchValue = "";
        $scope.users = response.data.users;
        $scope.videos = response.data.videos;
    })
})