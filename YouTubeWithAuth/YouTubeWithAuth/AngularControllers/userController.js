﻿app.controller('userController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id;

    $http({
        method: 'POST',
        url: 'Videos/getUserVideos',
        data: {
            id: id
        }
    }).then(function (response) {
        $scope.userVideos = response.data;
    })

    $http({
        method: 'POST',
        url: 'Videos/getUser',
        data: {
            id: id
        }
    }).then(function (response) {
        $scope.username = response.data.username;
        $scope.profileURL = response.data.profileURL;
    })

    $http({
        method: 'POST',
        url: 'Videos/getUserPlaylists',
        data: {
            id: id
        }
    }).then(function (response) {
        $scope.playlists = response.data;
    })
});