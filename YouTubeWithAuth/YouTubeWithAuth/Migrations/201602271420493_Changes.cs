namespace YouTubeWithAuth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "VideoID", "dbo.Videos");
            DropIndex("dbo.Comments", new[] { "VideoID" });
            RenameColumn(table: "dbo.Tags", name: "VideoID", newName: "TagID");
            RenameColumn(table: "dbo.Comments", name: "VideoID", newName: "Video_Id");
            RenameIndex(table: "dbo.Tags", name: "IX_VideoID", newName: "IX_TagID");
            AlterColumn("dbo.Comments", "Video_Id", c => c.Int());
            CreateIndex("dbo.Comments", "Video_Id");
            AddForeignKey("dbo.Comments", "Video_Id", "dbo.Videos", "Id");
            DropColumn("dbo.Videos", "VideoID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Videos", "VideoID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Comments", "Video_Id", "dbo.Videos");
            DropIndex("dbo.Comments", new[] { "Video_Id" });
            AlterColumn("dbo.Comments", "Video_Id", c => c.Int(nullable: false));
            RenameIndex(table: "dbo.Tags", name: "IX_TagID", newName: "IX_VideoID");
            RenameColumn(table: "dbo.Comments", name: "Video_Id", newName: "VideoID");
            RenameColumn(table: "dbo.Tags", name: "TagID", newName: "VideoID");
            CreateIndex("dbo.Comments", "VideoID");
            AddForeignKey("dbo.Comments", "VideoID", "dbo.Videos", "Id", cascadeDelete: true);
        }
    }
}
