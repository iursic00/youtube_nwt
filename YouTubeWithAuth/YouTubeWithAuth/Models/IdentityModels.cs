﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using YouTube.Models;

namespace YouTubeWithAuth.Models
{
    public enum GenderType
    {
        Male, Female
    }
    // You can add profile data for the user by adding more properties to your YouTubeUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class YouTubeUser : IdentityUser
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [DataType(DataType.Date, ErrorMessage = "The value must be date!")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Birthday { get; set; }

        public GenderType Gender { get; set; }

        public string Location { get; set; }

        public string AvatarURL { get; set; }

        public virtual ICollection<Video> UserVideos { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Playlist> Playlists { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<YouTubeUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<YouTubeUser>
    {
        public DbSet<Video> Videos { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Playlist> Playlists { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
       


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
       

    }
}