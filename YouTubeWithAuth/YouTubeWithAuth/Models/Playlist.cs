﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using YouTubeWithAuth.Models;

//Playlista(lista videa s nekim nazivom)
namespace YouTube.Models
{
    public class Playlist
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlaylistId { get; set; }

        [Required]
        public string NamePlaylist { get; set; }

        public virtual ICollection<Video> Videos { get; set; }

        [StringLength(128), MinLength(3)]
        [ForeignKey("YouTubeUser")]
        public string Id { get; set; }
        public virtual YouTubeUser YouTubeUser { get; set; }

    }
}