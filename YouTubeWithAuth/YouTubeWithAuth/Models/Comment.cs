﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YouTubeWithAuth.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }

        [Required]
        public string CommentText { get; set; }

        public int? LikesCounter { get; set; }

        public int? DislikesCounter { get; set; }

        [Required]
        public DateTime CommentDate { get; set; }

        [ForeignKey("YouTubeUser")]
        public string Id { get; set; }
        public virtual YouTubeUser YouTubeUser { get; set; }

        [ForeignKey("Video")]
        public int VideoID { get; set; }
        public virtual Video Video { get; set; }
    }
}