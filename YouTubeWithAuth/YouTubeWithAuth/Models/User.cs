﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YouTube.Models
{

    public enum GenderType
    {
        Male, Female
    }

    public class User
    {

        public int UserID { get; set; }

        [Required]
        [StringLength(30, ErrorMessage ="Username must have minimum 4 characters at least 30",MinimumLength =4)]
        public string Username { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(30,ErrorMessage ="Password must have at least 6 characters and maximum 30 characters", MinimumLength =6)]
        public string Password { get; set; }

        [DataType(DataType.EmailAddress,ErrorMessage ="The value must be eamil adress!")]
        public string Email { get; set; }

        [DataType(DataType.Date, ErrorMessage ="The value must be date!")]
        public DateTime? Birthday { get; set; }

        public GenderType Gender { get; set; }

        [DataType(DataType.PhoneNumber, ErrorMessage ="The value must be phone number!")]
        public string PhoneNumber { get; set; }


        public string Location { get; set; }

        public string Avatar { get; set; }

        public virtual List<Video> UserVideos { get; set; }

    }


}