﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YouTubeWithAuth.Models
{
    public class UploadVideoViewModel
    {
        [Required]
        [StringLength(40, ErrorMessage = "Maximum length is 40 and minimum length is 4!", MinimumLength = 4)]
        public string Title { get; set; }


        [MaxLength(250, ErrorMessage = "Maximum length is 250 characters")]
        public string Description { get; set; }

        public bool Movie { get; set; }

        public bool Music { get; set; }

        public bool Art { get; set; }

        public bool News { get; set; }

        public bool Family { get; set; }

        public bool Celebrity { get; set; }

        [Required]
        public HttpPostedFileBase thumbnail { get; set; }

        [Required]
        public HttpPostedFileBase video { get; set; }

    }

    public class MostViwedVideos
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string imageURL { get; set; }
        public int? NoViews { get; set; }

    }
    public class MainPageVideos
    {
        public int id { get; set; }
        public string imageURL { get; set; }
        public string title  { get; set; }
        public string description { get; set; }
        public string username { get; set; }
        public string userID { get; set; }
        public int? noViews { get; set; }
        public int? noLikes { get; set; }
    }

    public class ShowVideo
    {
        public string videoURL { get; set; }
        public string imageURL { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string usernameV { get; set; }
        public int? noViews { get; set; }
        public int? noLikes { get; set; }
        public int? noDislikes { get; set; }
        public List<EComment> Usercomments { get; set; }

    }
    public class EComment
    {
        public int id { get; set; }

        public string commentText { get; set; }

        public int? likesCounter { get; set; }

        public int? dislikesCounter { get; set; }

        public DateTime commentDate { get; set; }

        public  string username { get; set; }
    }

    public class SideVideo
    {
        public int id { get; set; }
        public string title { get; set; }
        public string imageURL { get; set; }
        public string username { get; set; }
        public int? views { get; set; }
    }

    public class UserPageProfile
    {
        public string username { get; set; }
        public string profileURL { get; set; }
        public string userID { get; set; }
    }

    public class searchResult
    {
        public List<SideVideo> videos { get; set; }
        public List<UserPageProfile> users { get; set; }
    }

    public class PlaylistsResult
    {
        public int id { get; set; }
        public string title { get; set; }
        public string image  { get; set; }
        public int noVideos { get; set; }
    }
}