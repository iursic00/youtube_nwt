﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouTubeWithAuth.Models
{
    public class Mapper
    {

        internal static EditProfileViewModel EditProfileViewModelMapper(YouTubeUser map)
        {
            var model = new EditProfileViewModel();

            model.FirstName = map.FirstName;
            model.LastName = map.LastName;
            model.UserName = map.UserName;
            model.Email = map.Email;
            model.PhoneNumber = map.PhoneNumber;
            model.Location = map.Location;
            model.AvatarURL = map.AvatarURL;

            return model;
        }

        internal static void EditProfileViewModelToYouTubeUser(YouTubeUser user, EditProfileViewModel model)
        {
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;
            user.PhoneNumber = model.PhoneNumber;
            user.Location = model.Location;
            user.AvatarURL = model.AvatarURL;
        }

        internal static MainPageVideos toMainPageVideos(Video video)
        {
            var temp = new MainPageVideos();
            temp.id = video.VideoId;
            temp.imageURL = video.CoverImageURL;
            temp.title = video.Title;
            temp.username = video.YouTubeUser.UserName;
            temp.noViews = video.ViewsCounter;
            temp.noLikes = video.LikesCounter;
            temp.description = video.Description;
            temp.userID = video.YouTubeUser.Id;

            return temp;
        }

        internal static MostViwedVideos toMostViwedVideos(Video video)
        {
            var temp = new MostViwedVideos();

            temp.id = video.VideoId;
            temp.Title = video.Title;
            temp.imageURL = video.CoverImageURL;
            temp.NoViews = video.ViewsCounter;

            return temp;
        }

        internal static ShowVideo toShowVideo(Video video)
        {
            var result = new ShowVideo();

            result.description = video.Description;
            result.imageURL = video.CoverImageURL;
            result.noDislikes = video.DislikesCounter;
            result.noLikes = video.LikesCounter;
            result.noViews = video.ViewsCounter;
            result.title = video.Title;
            result.usernameV = video.YouTubeUser.UserName;
            result.videoURL = video.VideoContentURL;

            return result;
        }

        internal static EComment toEComment(Comment comm)
        {
            var temp = new EComment();

            temp.id = comm.CommentId;
            temp.commentDate = comm.CommentDate;
            temp.commentText = comm.CommentText;
            temp.dislikesCounter = comm.DislikesCounter;
            temp.likesCounter = comm.LikesCounter;
            temp.username = comm.YouTubeUser.UserName;

            return temp;
        }
    }
}