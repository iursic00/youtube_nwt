﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using YouTube.Models;

namespace YouTubeWithAuth.Models
{
    public class Video
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VideoId { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Maximum length is 40 and minimum length is 4!", MinimumLength = 4)]
        public string Title { get; set; }

        [Required]
        public string VideoContentURL { get; set; }

        [Required]
        public string CoverImageURL { get; set; }

        [MaxLength(250, ErrorMessage = "Maximum length is 250 characters")]
        public string Description { get; set; }

        public int? ViewsCounter { get; set; }

        public int? LikesCounter { get; set; }

        public int? DislikesCounter { get; set; }

        public bool? Movie { get; set; }

        public bool? Music { get; set; }

        public bool? Art { get; set; }

        public bool? News { get; set; }

        public bool? Family { get; set; }

        public bool? Celebrity { get; set; }

        //Svaki video mora imati vlasnika 
        [StringLength(128), MinLength(3)]
        [ForeignKey("YouTubeUser")]
        public string Id { get; set; }

        public virtual YouTubeUser YouTubeUser { get; set; }

        //Svaki video može imati komentare        

       public virtual ICollection<Comment> UserComments { get; set; }

        public virtual ICollection<Playlist> Playlist { get; set; }
    }
}